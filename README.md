# gnome-shell-extension-dash-to-dock

This extension enhances the dash moving it out of the overview and transforming it in a dock for an easier launching of applications and a faster switching between windows and desktops without having to leave the desktop view.

Extension code:

https://github.com/micheleg/dash-to-dock

<br><br>
How to clone this repository:

 ```
 git clone https://github.com/micheleg/dash-to-dock.git
 ```
 
